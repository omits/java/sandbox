package lesson07;

public class Pizza {
    private String name;
    Integer size;
    public Pizza(String name, Integer size) {
        this.name = name;
        this.size = size;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (!(obj instanceof Pizza p)) return false;

        return this.size.equals(p.size)
                && this.name.equals(p.getName());
    };
}
