package elena;

public class CW02 {

        public static int countingValleys(int steps, String path) {
            int curr = 0;
            int prev = 0;
            int counter = 0;
            for (char c: path.toCharArray()) {
                switch (c) {
                    case 'U': prev = curr; curr += 1; break;
                    case 'D': prev = curr; curr -= 1; break;
                }
                if (curr == 0 && prev < 0) counter ++;
            }
            return counter;
        }

}
