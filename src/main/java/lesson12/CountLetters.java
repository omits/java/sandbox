package lesson12;

import java.util.HashMap;
import java.util.Map;

public class CountLetters {
    static Map<Character, Integer> countLettersV1(String line) {
        HashMap<Character, Integer> result = new HashMap<>();
        for(char c: line.toCharArray()) {
            if (!result.containsKey(c)) {
                result.put(c, 1);
            } else {
                int count = result.get(c);
                result.put(c, count + 1);
            }
        }

        return result;
    }

    static Map<Character, Integer> countLettersV2(String line) {
        HashMap<Character, Integer> result = new HashMap<>();
        for(char c: line.toCharArray()) {
            int count = result.getOrDefault(c, 0);
            result.put(c, count + 1);
        }

        return result;
    }
    static Map<Character, Integer> countLettersV3(String line) {
        HashMap<Character, Integer> result = new HashMap<>();
        for(char c: line.toCharArray()) {
            result.merge(c, 1, Integer::sum);
        }

        return result;
    }

    static Map<Character, Integer> countLettersV4(String line) {
        Counter<Character> counter = new Counter<Character>();
        for(char c: line.toCharArray()) {
            counter.count(c);
        }

        return counter.get();
    }
    public static void main(String[] args) {
        String line = "Hello worlds";
        Map<Character, Integer> result = countLettersV4(line);
        System.out.println(result);
    }
}
