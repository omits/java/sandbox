package lesson02;

import java.io.InputStream;
import java.util.Scanner;

public class Input {
    public static void main(String[] args) {
        InputStream in = System.in;
        Scanner scanner = new Scanner(in);

        System.out.print("Input: ");
        String line = scanner.nextLine();

        try {
            int x = Integer.parseInt(line);
            System.out.printf("Value %d was successfully parsed!", x);
        } catch (Exception ex) {
            System.out.printf("%s\nCannot parse string: %s", ex, line);
        }
    }
}
