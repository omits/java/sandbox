package lesson13;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class HandmadeCollector {
    public static void main(String[] args) {
        Arrays.asList(1, 2, 3)
                .stream()
                .map(x -> x * 10)
                .collect(new Collector<Integer, List<Integer>, Set<Integer>>() {

                    @Override
                    public Supplier<List<Integer>> supplier() {
                        return () -> new ArrayList<>();
                    }

                    @Override
                    public BiConsumer<List<Integer>, Integer> accumulator() {
                        return List::add;
                    }

                    @Override
                    public BinaryOperator<List<Integer>> combiner() {
                        return (l1, l2) -> {
                            var l3 = new ArrayList<Integer>();
                            l3.addAll(l1);
                            l3.addAll(l2);
                            return l3;
                        };
                    }

                    @Override
                    public Function<List<Integer>, Set<Integer>> finisher() {
                        return HashSet::new;
                    }

                    @Override
                    public Set<Characteristics> characteristics() {
                        return new HashSet<>();
                    }
                });
    }
}
