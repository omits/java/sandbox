package lesson13;

import lesson12.Counter;
import libs.EX;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class HashTables {
    public static Map<String, Long> toMap(List<String> words) {
        return words
                .stream()
                .collect(groupingBy(w -> w, counting()));
    }

    public static Map<String, Integer> toMap1(List<String> words) {
        Counter<String> c = new Counter<>();
        words.forEach(c::count);
        return c.get();
    }

    public static Boolean checkMagazine(List<String> magazine, List<String> note) {
        Map<String, Long> mapMagazine = toMap(magazine);
        Map<String, Long> mapNote = toMap(note);

        return mapNote
                .keySet()
                .stream()
                .allMatch(w -> mapMagazine.getOrDefault(w, 0L) >= mapNote.get(w));
    }

    public static void main(String[] args) {
        List<String> a = Arrays.asList("give", "me", "one", "grand", "today", "night");
        List<String> b = Arrays.asList("give", "one", "grand", "today");

        System.out.println(checkMagazine(a, b));
    }
}
