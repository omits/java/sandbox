package lesson09;

public class ArrayApplication {
    public static void main(String[] args) {
        Array a = new ArrayInMemory();
        System.out.println(a); // []
        a.add(7);
        System.out.println(a); // [7]
        a.add(5);
        a.add(15);
        a.add(8);
        System.out.println(a); // [7, 5, 15, 8]
        System.out.println(a.length()); // 4
        a.removeLast();
        a.removeAt(1);
        System.out.println(a); // [7, 15]
        System.out.println(a.get(0)); // 7
        System.out.println(a.get(1)); // Exception
    }
}
