package lesson09;

public class ArrayInMemory implements Array {
    private int[] data;
    private int next;
    private final double  growFactor;
    private final static int DEFAULT_SIZE = 16;
    private final static double DEFAULT_GROW_FACTOR = 1.5;

    public ArrayInMemory(int size, double growFactor) {
        this.data = new int[size];
        this.growFactor = growFactor;
        this.next = 0;
    }

    public ArrayInMemory(int size) {
        this(size, DEFAULT_GROW_FACTOR);
    }

    public ArrayInMemory() {
        this(DEFAULT_SIZE);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < next; i++) {
            if (i > 0) sb.append(", ");
            sb.append(data[i]);
        }
        sb.append("]");
        return sb.toString();
    }

    @Override
    public int length() { return next; }

    private int calcGrowth() {
        return (int)(data.length * growFactor + 1);
    }

    private void grow() {
        int[] data2 = new int[calcGrowth()];
        System.arraycopy(data, 0, data2, 0, data.length);
        data = data2;
    }
    private void ensureSizeEnough() {
        if (next > data.length) grow();
    }

    @Override
    public void add(int x) {
        data[next] = x;
        next = next + 1;
    }

    @Override
    public int get(int idx) {
        if (idx > next) throw new IndexOutOfBoundsException();
        return data[idx];
    }

    @Override
    public void removeLast() {
        if (next == 0) throw new IllegalStateException("Array is empty");
        next = next - 1;
    }

    @Override
    public void removeAt(int idx) {
        if (idx < 0) throw new IllegalStateException("index must be positive");
        if (idx > next) throw new IllegalStateException("index must be less then array length");
        System.arraycopy(
                data, idx + 1,
                data, idx,
                data.length - idx - 1
        );
        next = next - 1;
    }
}
