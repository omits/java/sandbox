package lesson09;

public interface Array {
    int length();

    void add(int x);

    int get(int idx);

    void removeLast();

    void removeAt(int idx);
}
