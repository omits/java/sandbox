package lesson14;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Stickers {
    static final IntStream data = IntStream.rangeClosed(173, 215);

    public static void main(String[] args) {
        Map<String, Long> test = Arrays.stream(data
                .mapToObj(String::valueOf)
                .collect(Collectors.joining())
                .split(""))
            .collect(Collectors.groupingBy(i -> i, Collectors.counting()));

        System.out.println(test);

    }
}
