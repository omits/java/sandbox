package lesson03;

import com.sun.source.doctree.SeeTree;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Lesson03 {
    public static void main(String[] args) {
        int[] anArray = { 100, 200, 300 };
        Arrays.stream(anArray).map(i -> i / 10).forEach(System.out::println);

        String s = "asdfqfqwe";
        byte[] bytes = s.getBytes();
        System.out.println(bytes.length);
        for (byte b:bytes) {
            System.out.printf("%d ", b);
        }

        System.out.println();

        IntStream target = s.chars();
        target.forEach(System.out::println);

//        Stream.generate(() -> )

//        Stream<Character> characterStream = s.chars().mapToObj(c -> (char) c);
//        characterStream.forEach(item -> System.out.printf("%s ", item));

    }
}
