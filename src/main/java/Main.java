import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        InputStream in = System.in;
        Scanner scanner = new Scanner(in);

        System.out.println("Enter your name:");

        String line = scanner.nextLine();
        String message = String.format("Hello, my dear %s!", line);
        System.out.println(message);
    }
}