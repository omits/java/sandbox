package lesson15;

import java.util.List;
import java.util.stream.Stream;

public class CombineApp {
    record Sentence(String subject, String verb, String object) {
        public static Sentence of(String subject, String verb, String object) {
            return new Sentence(subject, verb, object);
        }

        @Override
        public String toString() {
            return String.format("%s %s %s", subject, verb, object);
        }
    }

    private static List<Sentence> combinations(List<String> subjects, List<String> verbs, List<String> objects) {
        return subjects.stream().flatMap(subj ->
                verbs.stream().flatMap(verb ->
                        objects.stream().map(obj ->
                                Sentence.of(subj, verb, obj)
                        )
                )
        ).toList();
    }
    private static List<Stream<Stream<Sentence>>> combinations2(List<String> subjects, List<String> verbs, List<String> objects) {
        return subjects.stream().map(subj ->
                verbs.stream().map(verb ->
                        objects.stream().map(obj ->
                                Sentence.of(subj, verb, obj)
                        )
                )
        ).toList();
    }

    public static void main(String[] args) {
        List<String> subjects = List.of("1", "2");
        List<String> verbs = List.of("1", "3");
        List<String> objects = List.of("4", "2");

        List<Sentence> result = combinations(subjects, verbs, objects);
        result.forEach(System.out::println);
    }
}
