package lesson15.solution;

public interface Identifiable {
    int id();
}
