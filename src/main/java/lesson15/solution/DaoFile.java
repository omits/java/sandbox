package lesson15.solution;

public class DaoFile<A extends Identifiable> implements DAO<A> {
    @Override
    public void save(A a) {

    }

    @Override
    public A load(int id) {
        return null;
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public void delete(A a) {

    }
}
