package lesson15.solution;

import lesson15.Pizza;

public class App {
    public static void main(String[] args) {
        Pizza p1 = new Pizza(1,"Marg", 12);

        DaoHashMap<Pizza> dao = new DaoHashMap<>();

        dao.save(p1);
        Pizza loaded = dao.load(2);
        System.out.println(loaded);
        dao.delete(1);
    }
}
