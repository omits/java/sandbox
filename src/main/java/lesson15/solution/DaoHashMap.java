package lesson15.solution;

import java.util.HashMap;
import java.util.Map;

public class DaoHashMap<A extends Identifiable> implements DAO<A>{
    private final Map<Integer, A> storage = new HashMap<>();

    @Override
    public void save(A a) {
        storage.put(a.id(), a);
    }

    @Override
    public A load(int id) {
        return storage.get(id);
    }

    @Override
    public void delete(int id) {
        storage.remove(id);
    }
}
