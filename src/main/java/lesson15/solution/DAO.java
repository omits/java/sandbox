package lesson15.solution;


public interface DAO<A extends Identifiable> {
    void save(A a);
    A load(int id);
    void delete(int id);
    default void delete(A a) {
        delete(a.id());
    };
}
