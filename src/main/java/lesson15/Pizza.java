package lesson15;

import lesson15.solution.Identifiable;

public record Pizza(int id, String name, int size) implements Identifiable {

    @Override
    public int id() {
        return this.id;
    }
}
