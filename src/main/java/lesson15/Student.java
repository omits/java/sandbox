package lesson15;

import lesson15.solution.Identifiable;

public record Student(int id, String name, String group) implements Identifiable {
    public int id(){ return this.id; }
}
